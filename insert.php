<?php
require_once __DIR__ . "/App/Utils/Loging.php";
require_once __DIR__ . "/App/Utils/SendHttp.php";
require_once __DIR__ . "/Config/config.php";
require_once __DIR__ . "/App/Utils/vendor/autoload.php";
require_once __DIR__ . "/App/Model/FuncDB.php";
require_once __DIR__ . "/App/Controller/SmsFunction.php";

$getTipe = strtolower($_REQUEST["opt"]);

switch($getTipe){
	case "sms":case "email":case "socmed":
		$json_request = json_decode(file_get_contents('php://input'), true);
		$SendData = new \App\Controller\SmsFunction;
		$SendData->insert_to_mq($json_request);
	break;
	
	default:
		$response = new \App\Controller\Response;
		$response->INVALID_PARAM();
}
exit;
?>