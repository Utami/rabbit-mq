<?php
//CONFIG URL RETRY TIMEOUT
$URLRETRY = "http://172.17.0.47:8015/index.php?opt=";
$URLINSERT = "http://172.17.0.47:8015/insert.php?opt=";
// CONFIGURATION DATABASE REPORT
$DB_REPORT_CONFIG = [
	'database_type' => 'mysql',
	'database_name' => '',
	'server'        => '172.17.0.65',
	'username'      => 'appservices',
	'password'      => 'timbukt00'
];
// CONFIGURATION LOG PATH AND NAME
//$APP_LOG = __DIR__."/../Logs/request-".date("Y-m-d").".log";
$APP_LOG = __DIR__."/../Logs/request_".date("Y-m")."_";

$APP_WHITELIST_CHECK = [
    "bcanotif" => FALSE
];
// CONFIGURATION MQ
$CONF_MQ_SMS = array(
	"host"	=> "172.17.0.47",
	"port"	=> "5672",
	"user"	=> "app",
	"pass"	=> "appP4ss",
	"vhost"	=> "/",
	"logging" => true
);
$CONF_MQ_EMAIL = array(
	"host"	=> "172.17.0.47",
	"port"	=> "5672",
	"user"	=> "app",
	"pass"	=> "appP4ss",
	"vhost"	=> "/",
	"logging" => true
);
$CONF_MQ_SOCMED = array(
	"host"	=> "172.17.0.47",
	"port"	=> "5672",
	"user"	=> "app",
	"pass"	=> "appP4ss",
	"vhost"	=> "/",
	"logging" => true
);
//If this is enabled you can see AMQP output on the CLI
define('AMQP_DEBUG', true);
define('AMQP_WITHOUT_SIGNALS', true);

// FOR CHECKING NUMBER PREFIX (SMS) EX: 62856
$APP_MAX_PROVIDER_PREFIX = 6;
$APP_MIN_PROVIDER_PREFIX = 4;

// FOR ENABLE/DISABLE CHECKING TOKEN
$APP_ACTIVE_TOKEN_UNLIMITED = FALSE;

// FOR COUNT TABLE 
$APP_TEMPDATA_COUNT = 5;
?>
