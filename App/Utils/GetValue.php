<?php
namespace App\Utils;

class GetValue{
	public static function getNameTable($arrData) {
		$serviceClient=$arrData["srvc"];
		$prefix=$arrData["prefix"];
		switch($serviceClient){
			case "otp": $rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"]."-prior"; break;
			default:
				switch(strtolower($prefix)){
					case "other": $rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"]; break;
					default:
						$rs="i-".$arrData["thn"]."_".$arrData["bln"]."-".$arrData["nmtbl"]."-".strtolower($prefix);
				}
		}// end switch
		
		return $rs;
	}
}
?>