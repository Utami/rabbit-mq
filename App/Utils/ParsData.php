<?php
namespace App\Utils;

class ParsData{
	//======================SMS=======================
	public static function parsSmsMq($arrData) {
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam=unserialize($dtParam);

		$channel	= (isset($arrData["channel"])) ? $arrData["channel"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$msisdn		= (isset($arrData["msisdn"])) ? $arrData["msisdn"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$sender		= (isset($arrData["sender"])) ? $arrData["sender"] : "";
		$msg		= (isset($arrData["message"])) ? $arrData["message"] : "";
		$modelsrvc	= (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$time_req	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$time_dbrecv= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$send_type	= (isset($arrData["sending_type"])) ? $arrData["sending_type"] : "";
		$send_param	= (isset($arrData["sending_param"])) ? $arrData["sending_param"] : "";
		$urlsent	= (isset($sendParam["url_client_status_sent"])) ? $sendParam["url_client_status_sent"] : "";
		$urldlvr	= (isset($sendParam["url_client_delivery_report"])) ? $sendParam["url_client_delivery_report"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_nm	= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$severity	= (isset($arrData["severity"])) ? $arrData["severity"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$f7_tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		$urlcallback= (isset($arrData["url_callback"])) ? $arrData["url_callback"] : "";
		$urlcallbackstssend= (isset($arrData["url_callback_stssend"])) ? $arrData["url_callback_stssend"] : "";
		$urlcallbackstsdlvr= (isset($arrData["url_callback_stsdlvr"])) ? $arrData["url_callback_stsdlvr"] : "";

		$rs = array(
			"channel" => $channel,"id_p_s_cfg" => $id_p_s_cfg,"id_c" => $id_c,
			"dest_number" => $msisdn,"sender_name" => $sender,"sms_send" => $msg,
			"model_service" => $modelsrvc,"time_req" => $time_req,"time_dbrecv" => $arrData["recvDate"],
			"time_startsend" => $time_req,"time_send" => "","time_sent" => "",
			"retry_tosend" => "1","replyCode" => $code_sms,"id_hss_d" => "0",
			"senderName" => $sender,"type_send" => $send_type,"sendingParam" => $send_param,
			"url_client_status_sent" => $urlsent,"url_client_delivery_report" => $urldlvr,"operator_prefix_db"=>$provider,
			"client_prefix_dbname"=>$client_nm,"client_code_serv"=>$client_id,"engine_name_recv"=>$servicetype,
			"severity"=>$severity,"i_tcp_user"=>$username,"i_tcp_pass"=>"",
			"time_limitsend"=>"","to_client_parameters"=>"","f7_tid"=>$f7_tid,
			"ref_id"=>$ref_id,"url_callback"=>$urlcallback,"url_callback_stssend"	=> $urlcallbackstssend,
			"url_callback_stsdlvr"	=> $urlcallbackstsdlvr
		);

		return $rs;
	}// end parsSmsMq

	public static function parsSmsDB($arrData) {
		$dtParam	= preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam	= unserialize($dtParam);
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$name_c		= (isset($arrData["division_name"])) ? $arrData["division_name"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$idst		= (isset($arrData["id-st"])) ? $arrData["id-st"] : "";
		$nmst		= (isset($arrData["name-st"])) ? $arrData["name-st"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$nm_p_s_cfg	= (isset($sendParam["prvdName"])) ? $sendParam["prvdName"] : "";
		$msisdn		= (isset($arrData["msisdn"])) ? $arrData["msisdn"] : "";
		$sender		= (isset($arrData["sender"])) ? $arrData["sender"] : "";
		$service_id_prvd = (isset($sendParam['service_id_prvd'])) ? $sendParam['service_id_prvd'] : "";
		$modelservice = (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$ip_remote_addr = (isset($arrData["ip_remote"])) ? $arrData["ip_remote"] : "";
		$subject	= (isset($arrData["subject"])) ? $arrData["subject"] : "";
		$message	= (isset($arrData["message"])) ? $arrData["message"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$dateNow	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$recvDate	= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_name= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		// $ref_expl	= explode("_", $ref_id);
		$ref_expl	= $ref_id;
		$f7tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";

		$rs = array(
			"id-c"=>$id_c,"name-c"=>$name_c,"id-u"=>"0","name-u"=>"","user_name_tcp"=>$username,
			"id-m"=>"0","id-st"	=>$idst,"name-st"=>$nmst,"id-c-ch"=>"0","name-c-ch"=>"",
			"id-p-s-cfg"=>$id_p_s_cfg,"name-p-s-cfg"=>$nm_p_s_cfg,"id-hss"=>"0","name-hss"=>"","id-hss-d"=>"0",
			"name-hss-d"=>"","id-hss-m"=>"0","dest_number"=>$msisdn,"smscno"=>"","shortnumber"=>$sender,
			"trx_id_get"=>"","service_id_prvd"=>$service_id_prvd,"model_service" => $modelservice,
			"ip_http_forwarded"=>"","ip_remote_addr"=>$ip_remote_addr,"name-sender"=>$sender,
			"sms_req"=>"","hide_sms"=>"0","subject"=>$subject,"sms_send"=>$message,"code_sms"=>$code_sms,
			"budget_code"=>"","unique_code"=>"","live_code"=>"0","status_send"=>"2","status_delivery"=>"3","retry_tosend"=>"1",
			"time_querecv"=>$dateNow,"time_dbrecv"=>$recvDate,"time_sched"=>$recvDate,"time_startsend"=>$dateNow,
			"time_send"=>"","time_sent"=>"","time_delivery"=>"","operator_prefix_db"=>$provider,
			"client_prefix_dbname"=>$client_name,"client_code_serv"=>$client_id,"engine_name_recv"=>$servicetype,
			"f7_tid"=>$f7tid,"ref_id"=>$ref_expl
		);

		return $rs;
	}// end parsSmsDB

	//======================End SMS=======================

	//======================EMAIL=======================

	public static function parsEmailMq($arrData) {
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam=unserialize($dtParam);

		$channel	= (isset($arrData["channel"])) ? $arrData["channel"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$sendto		= (isset($arrData["email"])) ? $arrData["email"] : "";
		$attch		= (isset($arrData["attachment"])) ? $arrData["attachment"] : "";
		$subject	= (isset($arrData["subject"])) ? $arrData["subject"] : "";
		$msg		= (isset($arrData["message"])) ? $arrData["message"] : "";
		$msgdetail	= (isset($arrData["detailmessage"])) ? $arrData["detailmessage"] : "";
		$time_req	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$time_dbrecv= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$modelsrvc	= (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$sender		= (isset($arrData["sender"])) ? $arrData["sender"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$send_type	= (isset($arrData["sending_type"])) ? $arrData["sending_type"] : "";
		$send_param	= (isset($arrData["sending_param"])) ? $arrData["sending_param"] : "";
		$urlsent	= (isset($sendParam["url_client_status_sent"])) ? $sendParam["url_client_status_sent"] : "";
		$urldlvr	= (isset($sendParam["url_client_delivery_report"])) ? $sendParam["url_client_delivery_report"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_nm	= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$severity	= (isset($arrData["severity"])) ? $arrData["severity"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$f7_tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		$urlcallback= (isset($arrData["url_callback"])) ? $arrData["url_callback"] : "";
		$urlcallbackstssend= (isset($arrData["url_callback_stssend"])) ? $arrData["url_callback_stssend"] : "";
		$urlcallbackstsdlvr= (isset($arrData["url_callback_stsdlvr"])) ? $arrData["url_callback_stsdlvr"] : "";


		$rs = array(
			"channel" => $channel,"id_p_s_cfg" => $id_p_s_cfg,"id_c" => $id_c,
			"sendto" => $sendto,"attachment" => $attch,"subject" => $subject,
			"sms_send" => $msg,"msgdetail" => $msgdetail,"time_req" => $time_req,"time_dbrecv" => $time_dbrecv,
			"time_startsend" => $time_req,"time_send" => "","time_sent" => "",
			"retry_tosend" => "1","retry_tosend" => "1","replyCode" => $code_sms,
			"id_hss_d" => "0","modelService" => $modelsrvc,"type_send" => $send_type,
			"sendingParam" => $send_param,"url_client_status_sent" => $urlsent,"url_client_delivery_report" => $urldlvr,
			"operator_prefix_db"=>$provider,"client_prefix_dbname"=>$client_nm,"client_code_serv"=>$client_id,
			"engine_name_recv"=>$servicetype,"severity"=>$severity,"i_tcp_user"=>$username,
			"i_tcp_pass"=>"","time_limitsend"=>"","to_client_parameters"=>"",
			"f7_tid"=>$f7_tid,"ref_id"=>$ref_id,"url_callback"=>$urlcallback,
			"url_callback_stssend"=>$urlcallbackstssend,"url_callback_stsdlvr"=>$urlcallbackstsdlvr
		);

		return $rs;
	}// end parsEmailMq

	public static function parsEmailDB($arrData) {
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam=unserialize($dtParam);
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$name_c		= (isset($arrData["division_name"])) ? $arrData["division_name"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$idst		= (isset($arrData["id-st"])) ? $arrData["id-st"] : "";
		$nmst		= (isset($arrData["name-st"])) ? $arrData["name-st"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$nm_p_s_cfg	= (isset($sendParam["prvdName"])) ? $sendParam["prvdName"] : "";
		$email		= (isset($arrData["email"])) ? $arrData["email"] : "";
		$sender		= (isset($arrData["sender"])) ? $arrData["sender"] : "";
		$service_id_prvd = (isset($sendParam['service_id_prvd'])) ? $sendParam['service_id_prvd'] : "";
		$modelservice = (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$ip_remote_addr = (isset($arrData["ip_remote"])) ? $arrData["ip_remote"] : "";
		$subject	= (isset($arrData["subject"])) ? $arrData["subject"] : "";
		$message	= (isset($arrData["message"])) ? $arrData["message"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$dateNow	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$recvDate	= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_name= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		// $ref_expl	= explode("_", $ref_id);
		$ref_expl	= $ref_id;
		$f7tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";

		$rs = array(
			"id-c"=>$id_c,"name-c"=>$name_c,"id-u"=>"0","name-u"=>"","user_name_tcp"=>$username,
			"id-m"=>"0","id-st"	=>$idst,"name-st"=>$nmst,"id-c-ch"=>"0","name-c-ch"=>"",
			"id-p-s-cfg"=>$id_p_s_cfg,"name-p-s-cfg"=>$sendParam["prvdName"],"id-hss"=>"0","name-hss"=>"","id-hss-d"=>"0",
			"name-hss-d"=>"","id-hss-m"=>"0","dest_number"=>$email,"smscno"=>"","shortnumber"=>$sender,
			"trx_id_get"=>"","service_id_prvd"=>$service_id_prvd,"model_service" => $modelservice,
			"ip_http_forwarded"=>"","ip_remote_addr"=>$ip_remote_addr,"name-sender"=>$sender,
			"sms_req"=>"","hide_sms"=>"0","subject"=>$subject,"sms_send"=>$message,"code_sms"=>$code_sms,
			"budget_code"=>"","unique_code"=>"","live_code"=>"0","status_send"=>"2","status_delivery"=>"3","retry_tosend"=>"1",
			"time_querecv"=>$dateNow,"time_dbrecv"=>$recvDate,"time_sched"=>$recvDate,"time_startsend"=>$dateNow,
			"time_send"=>"","time_sent"=>"","time_delivery"=>"","operator_prefix_db"=>$provider,
			"client_prefix_dbname"=>$client_name,"client_code_serv"=>$client_id,"engine_name_recv"=>$servicetype,
			"f7_tid"=>$f7tid,"ref_id"=>$ref_expl
		);

		return $rs;
	}// end parsEmailDB

	//======================End EMAIL=======================

	//======================Socmed=======================

	public static function parsSocmedMq($arrData) {
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam=unserialize($dtParam);

		$channel	= (isset($arrData["channel"])) ? $arrData["channel"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$attch		= (isset($arrData["attachment"])) ? $arrData["attachment"] : "";
		$token		= (isset($arrData["token"])) ? $arrData["token"] : "";
		$secret_tkn	= (isset($arrData["secret_token"])) ? $arrData["secret_token"] : "";
		$media		= (isset($arrData["media"])) ? $arrData["media"] : "";
		$social_type= (isset($arrData["social_type"])) ? $arrData["social_type"] : "";
		$chatid		= (isset($arrData["chatid"])) ? $arrData["chatid"] : "";
		$msg_type	= (isset($arrData["message_type"])) ? $arrData["message_type"] : "";
		$location	= (isset($arrData["location"])) ? $arrData["location"] : "";
		$subject	= (isset($arrData["subject"])) ? $arrData["subject"] : "";
		$message	= (isset($arrData["message"])) ? $arrData["message"] : "";
		$time_req	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$time_dbrecv= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$modelsrvc	= (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$send_type	= (isset($arrData["sending_type"])) ? $arrData["sending_type"] : "";
		$send_param	= (isset($arrData["sending_param"])) ? $arrData["sending_param"] : "";
		$urlsent	= (isset($sendParam["url_client_status_sent"])) ? $sendParam["url_client_status_sent"] : "";
		$urldlvr	= (isset($sendParam["url_client_delivery_report"])) ? $sendParam["url_client_delivery_report"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_nm	= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$severity	= (isset($arrData["severity"])) ? $arrData["severity"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$f7_tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		$urlcallback= (isset($arrData["url_callback"])) ? $arrData["url_callback"] : "";
		$urlcallbackstssend= (isset($arrData["url_callback_stssend"])) ? $arrData["url_callback_stssend"] : "";
		$urlcallbackstsdlvr= (isset($arrData["url_callback_stsdlvr"])) ? $arrData["url_callback_stsdlvr"] : "";

		$rs = array(
			"channel" => $channel,"id_p_s_cfg" => $id_p_s_cfg,"id_c" => $id_c, "attachment" => $attch,
			"token" => $token,"secret_token" => $secret_tkn,"media" => $media,
			"social_type" =>  $social_type,"chatid" => $chatid,"message_type" => $msg_type,
			"location" => $location,"subject" => $subject,"message" => $message,
			"time_req" => $time_req,"time_dbrecv" => $time_dbrecv,"time_startsend" => $time_req,
			"time_send" => "","time_sent" => "","retry_tosend" => "1",
			"replyCode" => $code_sms,"id_hss_d" => "0","modelService" => $modelsrvc,
			"type_send" => $send_type,"sendingParam" => $send_param,"url_client_status_sent" => $urlsent,
			"url_client_delivery_report" => $urldlvr,"operator_prefix_db"=>$provider,"client_prefix_dbname"=>$client_nm,
			"client_code_serv"=>$client_id,"engine_name_recv"=>$servicetype,"severity"=>$severity,
			"i_tcp_user"=>$username,"i_tcp_pass"=>"","time_limitsend"=>"",
			"to_client_parameters"=>"","f7_tid"=>$f7_tid,"ref_id"=>$ref_id,
			"url_callback"=>$urlcallback,"url_callback_stssend"=>$urlcallbackstssend,"url_callback_stsdlvr"=>$urlcallbackstsdlvr
		);

		return $rs;
	}// end parsSocmedMq

	public static function parsSocmedDB($arrData) {
		$dtParam = preg_replace_callback('!s:(\d+):"(.*?)";!', function($m) { return 's:'.strlen($m[2]).':"'.$m[2].'";'; }, $arrData["sending_param"]);
		$sendParam=unserialize($dtParam);
		$id_c		= (isset($arrData["division_id"])) ? $arrData["division_id"] : "";
		$name_c		= (isset($arrData["division_name"])) ? $arrData["division_name"] : "";
		$username	= (isset($arrData["username"])) ? $arrData["username"] : "";
		$idst		= (isset($arrData["id-st"])) ? $arrData["id-st"] : "";
		$nmst		= (isset($arrData["name-st"])) ? $arrData["name-st"] : "";
		$id_p_s_cfg	= (isset($arrData["id-p-s-cfg"])) ? $arrData["id-p-s-cfg"] : "";
		$nm_p_s_cfg	= (isset($sendParam["prvdName"])) ? $sendParam["prvdName"] : "";
		$sendto		= (isset($arrData["chatid"])) ? $arrData["chatid"] : "";
		$sender		= (isset($arrData["sender"])) ? $arrData["sender"] : "";
		$service_id_prvd = (isset($sendParam['service_id_prvd'])) ? $sendParam['service_id_prvd'] : "";
		$modelservice = (isset($arrData["modelservice"])) ? $arrData["modelservice"] : "";
		$ip_remote_addr = (isset($arrData["ip_remote"])) ? $arrData["ip_remote"] : "";
		$subject	= (isset($arrData["subject"])) ? $arrData["subject"] : "";
		$message	= (isset($arrData["message"])) ? $arrData["message"] : "";
		$code_sms	= (isset($arrData["code_sms"])) ? $arrData["code_sms"] : "";
		$dateNow	= (isset($arrData["dateNow"])) ? $arrData["dateNow"] : "";
		$recvDate	= (isset($arrData["recvDate"])) ? $arrData["recvDate"] : "";
		$provider	= (isset($arrData["provider"])) ? $arrData["provider"] : "";
		$client_name= (isset($arrData["client_name"])) ? $arrData["client_name"] : "";
		$client_id	= (isset($arrData["client_id"])) ? $arrData["client_id"] : "";
		$servicetype= (isset($arrData["servicetype"])) ? $arrData["servicetype"] : "";
		$ref_id		= (isset($arrData["ref_id"])) ? $arrData["ref_id"] : "";
		// $ref_expl	= explode("_", $ref_id);
		$ref_expl	= $ref_id;
		$f7tid		= (isset($arrData["f7_tid"])) ? $arrData["f7_tid"] : "";
		$rs = array(
				"id-c"=>$id_c,"name-c"=>$name_c,"id-u"=>"0","name-u"=>"","user_name_tcp"=>$username,
				"id-m"=>"0","id-st"	=>$idst,"name-st"=>$nmst,"id-c-ch"=>"0","name-c-ch"=>"",
				"id-p-s-cfg"=>$id_p_s_cfg,"name-p-s-cfg"=>$sendParam["prvdName"],"id-hss"=>"0","name-hss"=>"","id-hss-d"=>"0",
				"name-hss-d"=>"","id-hss-m"=>"0","dest_number"=>$sendto,"smscno"=>"","shortnumber"=>$sender,
				"trx_id_get"=>"","service_id_prvd"=>$service_id_prvd,"model_service" => $modelservice,
				"ip_http_forwarded"=>"","ip_remote_addr"=>$ip_remote_addr,"name-sender"=>$sender,
				"sms_req"=>"","hide_sms"=>"0","subject"=>$subject,"sms_send"=>$message,"code_sms"=>$code_sms,
				"budget_code"=>"","unique_code"=>"","live_code"=>"0","status_send"=>"2","status_delivery"=>"3","retry_tosend"=>"1",
				"time_querecv"=>$dateNow,"time_dbrecv"=>$recvDate,"time_sched"=>$recvDate,"time_startsend"=>$dateNow,
				"time_send"=>"","time_sent"=>"","time_delivery"=>"","operator_prefix_db"=>$provider,
				"client_prefix_dbname"=>$client_name,"client_code_serv"=>$client_id,"engine_name_recv"=>$servicetype,
				"f7_tid"=>$f7tid,"ref_id"=>$ref_expl
			);

		return $rs;
	}// end parsEmailDB
	//======================End Socmed=======================

	public static function CreateJson($arrJson){
		$jencode=json_encode($arrJson);
		return $jencode;
	}
}// end class ParsData
?>
