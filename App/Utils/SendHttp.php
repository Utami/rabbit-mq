<?php
namespace App\Utils;

class SendHttp{
	function sendBackData($arrData, $urldata){
		$maxtime="30"; $maxsize="4096";$maxConnect='15';$maxtryconencttime='';
		$data_string = $arrData; //json_encode($arrData);
		$ch = curl_init();
		$keepAlive=TRUE;
		
		if($keepAlive){
			$GLOBALS['chCurl']=$ch;
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 300";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			curl_setopt($ch, CURLOPT_MAXCONNECTS, $maxConnect);  // limiting connections
			curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		}
		if($maxtryconencttime == ''){
			switch($maxtime){
				case ($maxtime > '30'): $maxtryconencttime='30'; break;
				case ($maxtime > '25' && $maxtime <= '30'): $maxtryconencttime='25'; break;
				case ($maxtime > '20' && $maxtime <= '25'): $maxtryconencttime='20'; break;
				case ($maxtime > '15' && $maxtime <= '20'): $maxtryconencttime='15'; break;
				case ($maxtime > '10' && $maxtime <= '15'): $maxtryconencttime='10'; break;
				case ($maxtime > '5' && $maxtime <= '10'): $maxtryconencttime='5'; break;
				default:
					$maxtryconencttime='2';
			}// end switch
		}
		$url = $urldata;
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_string)));
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $maxtime); // times out after $maxtime
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $maxtryconencttime); // times out after $maxtryconencttime
		$result = curl_exec($ch);//print_r($rs);
		$cont_type="";$response="";
		if(curl_errno($ch) == "0"){
		  $ret['error']=0;
		  $temp=$this->chunkedresult($result);
		  /*if(!((preg_match('/2[0-9][0-9]/',$temp['http_status'],$match)) OR ($temp['http_status'] == ""))){
		    $response=$temp['http_status_string'];
		  }else{
		    $response=$temp['body'];
			$cont_type=$temp['cont_type'];
		  }*/
		  $response=$temp['body'];
		}else{
		  $ret['error']=1;
		  $ret['full_iod']=$url;
		  $response="Error/Connection Failed";
		}
		$ret['full_iod']=$url;
		$ret['trx_date']=date('YmdHis');
		$ret['response']=trim($response);
		$ret['cont_type']=$cont_type;
		if(!($keepAlive)){ curl_close($ch); }
		return $ret;
	}
	function chunkedresult($data) {
		$cont_type = "";
		$http_status_string = "";$http_status="";
		$temp = explode("\r\n\r\n",$data);
		$countPars = count($temp);
		$doChunk = "0";
		if ($countPars > 1) {
			$ret['header'] = trim($temp[0]);
			if ((preg_match('/<html>(.*)/i',trim($ret['header']),$match)) AND (preg_match('/<body>(.*)/i',trim($ret['header']),$match))){
				$ret['body'] = $data;
			}// end if preg_match
			else {
				$doChunk = "1";
				$ret['body']   = str_replace($ret['header'],"",$data);
				$statusTemp = "";
				if (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)\\r\\n/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}elseif (preg_match('/^HTTP\/1\.[0-1] ([0-9][0-9][0-9] .*)/', $ret['header'], $match)) {
					$statusTemp = $match[1];
				}// end if preg_match
			}// end if else preg_match
			$status = explode(" ",trim($statusTemp));
			if (isset($status[0])){ $http_status = $status[0]; }else{ $http_status = ""; }
			if (isset($match[0])){ $http_status_string = trim($match[0]); }else{ $http_status_string = ""; }
			if (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)\\r\\n/',$ret['header'],$match)) { $cont_type = $match[1]; }elseif (preg_match('/Content\\-Type:\\s+([a-zA-Z\/]*)/',$ret['header'],$match)) { $cont_type = $match[1]; }// end if preg_match
			if ((strlen($cont_type) != (strlen(str_replace("xml","",$cont_type)))) AND ($cont_type != "")) { $cont_type = "xml"; }
		}// end if countPars
		if ($doChunk == "0") {
			$body = $data;
			if (substr(trim($data),0,5) == "<?xml") { $cont_type = "xml"; }
			if (preg_match('/^<html>(.*)<\/html>$/i',trim($data),$match)) {
				if (strlen(trim("<html>".$match[1]."</html>")) == strlen(trim($data)) ) {
					if (preg_match('/<body>(.*)<\/body>/i',trim($data),$match)){ $body = $match[1]; }
				}// end if strlen
			}// end if preg_match
			$ret['body'] = trim($body);
			$ret['header'] = "";
		}// end if doChunk
		$ret['http_status'] = $http_status;
		$ret['http_status_string'] = $http_status_string;
		if ($cont_type == ""){ $cont_type = "text/html"; }
		$ret['cont_type'] = $cont_type;

		return $ret;
	}// end chunkedresult
}
?>
