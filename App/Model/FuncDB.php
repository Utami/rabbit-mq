<?php

namespace App\Model;
use \App\Utils\Loging;
use Exception;

class FuncDB {

	protected $logfile;
	
	public function __construct () {
		$this->logfile	= new \App\Utils\Loging;
	}

	public function connectDb($db, $data){
		$rs=false;$nmdatabase="";
		if (isset($db)){
			$svrdb	= $data["server"];
			$usrdb	= $data["username"];
			$passdb	= $data["password"];
			$conn	= mysqli_connect($svrdb,$usrdb,$passdb,$db);
			if(!$conn){
				$this->logfile->write(__FUNCTION__, "Error: Unable to connect to MySQL. ".$db, $GLOBALS["logname"]);
				$this->logfile->write(__FUNCTION__, "Debugging errno: " . mysqli_connect_errno() . PHP_EOL, $GLOBALS["logname"]);
				$this->logfile->write(__FUNCTION__, "Debugging error: " . mysqli_connect_error() . PHP_EOL, $GLOBALS["logname"]);
			}else{
				$rs	= $conn;
			}
		}else{
			$this->logfile->write(__FUNCTION__, "Dbname is empty", $GLOBALS["logname"]);
		}
		return $rs;
	}// end connectDb
	
	function closeDB($conn){ mysqli_close($conn); }// end closeDb
	
	function queryDB($q, $conn){
		$rs=false;
		if(isset($q)){
			$query	= mysqli_query($conn, $q);
			if($query){ $rs	= $query; }
			else{
				$this->logfile->write(__FUNCTION__, "Failed Query : ".$q, $GLOBALS["logname"]);
			}
		}else{
			$this->logfile->write(__FUNCTION__, "Query is empty", $GLOBALS["logname"]);
		}
		
		return $rs;
	}// end queryDB
	
	function rowDB($q){
		$rs=array();
		while($r=mysqli_fetch_array($q)){ array_push($rs, $r); }// end while
		mysqli_free_result($q);
		
		return $rs;
	}//end rowDB
}
?>