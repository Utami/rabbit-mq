<?php

namespace App\Model;
use \App\Utils\Loging;
use \App\Utils\Medoo;
use Exception;

class Databases {

	private $conn_report;
	
	public function close_conn_db_report () {
		$this->conn_report		= null;
	}
	
	public function connection_db_report () {
		$this->conn_report		= $this->connection("DB_REPORT_CONFIG");
		return $this->conn_report;
	}

	public function connection ($index_config) {
		$loging = new \App\Utils\Loging;
		//$response = new \App\Controller\Response;
		if (isset($GLOBALS[$index_config])) {
			try {
				return new \App\Utils\Medoo($GLOBALS[$index_config]);
			}
			catch (Exception $e) {
				$loging->write(
					"databases", 
					"$index_config failed connect\n".$e->getMessage().$e->getTraceAsString(),
					$GLOBALS["logname"]
				);
				//$response->INTERNAL_ERROR();
			}
		} 
		else {
			$loging->write("databases", "$index_config not set on config");
			//$response->INTERNAL_ERROR();
		}
	}

}

?>