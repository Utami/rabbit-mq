<?php

namespace App\Controller;
use \App\Utils\Loging;

class Response extends \App\Utils\Loging {

    public function SUCCESS () {
        $R = "0";
        $this->ENDED(__FUNCTION__, $R);
    }
	
	public function INVALID_PARAM () {
        $R = "1";
        $this->ENDED(__FUNCTION__, $R);
    }

    private function ENDED ($STATUS, $R) {
        $ref_id = "";
        $code_sms = "";
        if (isset($GLOBALS["ref_id"])) $ref_id = $GLOBALS["ref_id"];

        $response = json_encode(array (
            "rc"        => $R,
            "ref_id"    => $ref_id,
        ));
        $this->write("RESPONSE", "$STATUS = $response", $GLOBALS["logname"]);
        header('Content-Type: application/json');
		echo $response;
        exit();
    }

}

?>
