<?php

namespace App\Controller;
use \App\Utils\Loging;
use \App\Model\FuncDB;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class SmsFunction extends \App\Utils\Loging {
	protected $db;
	
	public function __construct () {
		$this->db	= new \App\Model\FuncDB;
	}

	protected function insert_to_recv ($arrDB) {
		$rs = false;
		$conn = $this->db->connectDb($arrDB["dbname"], $GLOBALS["DB_REPORT_CONFIG"]);
		if($conn){
			$qset = "SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED ;";
			$this->db->queryDB($qset, $conn);
			$query = "";
			foreach($arrDB["arrdata"] as $key => $val){
				$query	.= "`".$key."`='".addslashes($val)."', ";
			}// end foreach
			$query	= substr($query, 0, -2);
			$q		= "INSERT INTO `".$arrDB["tablename"]."` SET ".$query;
			$inDB	= $this->db->queryDB($q, $conn);
			$this->db->closeDB($conn);
			if($inDB){ $rs = true; }
		}
		
		return $rs;
	}// end insert_to_recv
		
	public function insert_to_mq ($arrData) {
		$dataMq = $arrData["datamq"];
		$dataDB = $arrData["datadb"];
		$GLOBALS["logname"] = $arrData["logname"];
		$connMQ	= new AMQPConnection($dataMq["confmq"]["host"], $dataMq["confmq"]["port"], $dataMq["confmq"]["user"], $dataMq["confmq"]["pass"], $dataMq["confmq"]["vhost"]);
		if($connMQ){
			$rs = $this->insert_to_recv($dataDB);
			if($rs){
				$channel= $connMQ->channel();
				$channel->basic_qos(0, 1, false);
				$dlvrmode =  array('delivery_mode' => 2);
				$msgmq	= new AMQPMessage($dataMq["jsonmq"], $dlvrmode);
				$channel->basic_publish($msgmq, $dataMq["router"], $dataMq["severity"]);
				
				$channel->close();
			}else{
				print_r("error");
			}
			$connMQ->close();
		}else{
			print_r("error");
		}
	}// end insert_to_mq
}
?>
