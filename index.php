<?php
error_reporting(1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
//ini_set("log_errors", 1);
require_once __DIR__ . "/App/Utils/Loging.php";
require_once __DIR__ . "/App/Utils/ParsData.php";
require_once __DIR__ . "/App/Utils/GetValue.php";
require_once __DIR__ . "/App/Utils/SendHttp.php";
require_once __DIR__ . "/Config/config.php";
require_once __DIR__ . "/App/Controller/Response.php";
require_once __DIR__ . "/App/Controller/sendToQueue.php";

$getTipe = strtolower($_REQUEST["opt"]);
$json_request = json_decode(file_get_contents('php://input'), true);
$SendData = new \App\Controller\sendToQueue;
switch($getTipe){
	case "sms":
		$SendData->setTipeData("sms");
		$SendData->setDataSend($json_request);
		$SendData->sendQueue();
	break;
	
	case "email":
		$SendData->setTipeData("email");
		$SendData->setDataSend($json_request);
		$SendData->sendQueue();
	break;
	
	case "socmed":
		$SendData->setTipeData("socmed");
		$SendData->setDataSend($json_request);
		$SendData->sendQueue();
	break;
	
	default:
		$response = new \App\Controller\Response;
		$response->INVALID_PARAM();
}
exit;
?>
